package main

import (
    "fmt"
    "bufio"
    "os"
    /*"io/ioutil"*/
    "path/filepath"
    /*"strings"*/
    "sort"
)

//var punctuation = []string{",", "-", ".", "'", "\"", "!", " ", ";", "\n", "?"};
var eRanking = []string{"e", "t", "a", "o", "i", "n", "s", "h", "r", "d", "l", "c", "u", "m", "w", "f", "g", "y", "p", "b", "v", "k", "j", "x", "q", "z"}

type analysis struct {
    char string
    frequency int
    relative_freq float64
}

type anaSlice []*analysis

func (d anaSlice) Len() int {
    return len(d)
}

func (d anaSlice) Swap(i, j int) {
    d[i], d[j] = d[j], d[i]
}

func (d anaSlice) Less(i, j int) bool {
    return d[i].relative_freq > d[j].relative_freq
}

func main() {
    fmt.Print("Enter file name: ")
    consoleScanner := bufio.NewScanner(os.Stdin)
    //filePath, err := consoleScanner.ReadString('\n')
    var filePath string
    if consoleScanner.Scan() {
        filePath = consoleScanner.Text()
    } else {
        filePath = ""
    }
    if err := consoleScanner.Err(); err != nil {
        fmt.Println(err)
        return
    }
    fmt.Printf("%q\n", filePath)
    //filePath = strings.TrimSpace(filePath)
    //fmt.Printf("%q\n", filePath)
    absFilePath, err := filepath.Abs(filePath)
    if err != nil {
        fmt.Println(err)
        return
    }
    fmt.Println("Trying to open file: ", absFilePath);
    text := ReadFile(absFilePath)
    data := RelativeFrequency(absFilePath)
    key := suggestKey(data)
    Decrypt(text, key)
}

func ReadFile(filePath string) string {
    var text string
    file, err := os.Open(filePath)
    if err != nil {
        fmt.Println(err)
        return ""
    }
    defer file.Close()
    fileScanner := bufio.NewScanner(file)
    for fileScanner.Scan() {
        text += fileScanner.Text()
    }
    fmt.Printf("%s", text);
    return text
}

func RelativeFrequency(filePath string) anaSlice {
    fmt.Println("Computing Relative Frequency for the file", filePath)
    frequency := make(map[string]int)
    relativeFrequency := make(map[string]float64)
    file, err := os.Open(filePath)
    if err != nil {
        fmt.Println(err)
    }
    defer file.Close()
    fileScanner := bufio.NewScanner(file)
    for fileScanner.Scan() {
        line := fileScanner.Text()
        //fmt.Println(line)
        for _, c := range line {
            frequency[string(c)]++
        }
    }
    var count int = 0
    fmt.Println("length of the array is: ", len(frequency))
    for k, v := range frequency {
        if int(k[0]) >= 97 && int(k[0]) <= 122 {
            //fmt.Printf("%s (%d) is an alphabet and frequency is %d\n", k, int(k[0]), v)
            count += v
        } else {
            //fmt.Printf("%s->%d\n", k, v);
        }
    }
    fmt.Println("Total number of alphabets are: ", count);
    for i, j := range frequency {
        if int(i[0]) >= 97 && int(i[0]) <= 122 {
            //fmt.Println(j)
            relativeFrequency[i] = float64(j)/float64(count)
        }
    }
    var sum float64
    for _, t := range relativeFrequency {
        //fmt.Printf("%s -> %f\n", f, t)
        sum += t * t
    }
    fmt.Printf("Sum of Relative Frequencies is: %f", sum)
    data := make(anaSlice, 0, len(relativeFrequency))
    for i, v := range relativeFrequency {
        pair := new(analysis)
        pair.char = i
        pair.frequency = frequency[i]
        pair.relative_freq = v
        data = append(data, pair)
    }
    sort.Sort(data)
    for _, d := range data {
        fmt.Printf("char: %s, freq: %d, rel_freq: %f \n", d.char, d.frequency, d.relative_freq)
    }
    return data
}

func suggestKey(data anaSlice) map[string]string {
    sort.IsSorted(data)
    // Key is plain -> cipher i.e key[plain] = cipher
    var key = make(map[string]string, 26)
    for i, d := range data {
        fmt.Printf("plain: %s -> cipher: %s\n", eRanking[i], d.char)
        key[eRanking[i]] = d.char
    }
    return key
}

/**
 * Encrypts the given text with the provided key
 * Not completely implemented
 */
func Encrypt(plaintext string, key map[string]string) {
    var cipherText string
    for _,char := range plaintext {
        fmt.Println(char)
        cipherText += key[string(char)]
    }
    fmt.Println(cipherText)
}

/**
 * Decrypts the text provided with the given key
 * and returns Plain text back
 */
func Decrypt(ciphertext string, key map[string]string) string {
    var plainText string
    for _, char := range ciphertext {
        if char >= 97 && char <= 122 {
            plainValue := GetKey(string(char), key)
            if plainValue != "" {
                plainText += plainValue
            }
        } else {
            plainText += string(char)
        }
    }
    fmt.Println(plainText)
    return plainText
}

/**
 * Returns the key mapped for the character
 * in the cipher text
 */
func GetKey(elem string, keymap map[string]string) string {
    for key, value := range keymap {
        if value == elem {
            return key
        }
    }
    return ""
}
